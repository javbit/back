const cors = require('cors');  // Instalar dependencia 'cors' con npm

const express = require('express');   //referencia al paquete express
//const userFile = require('./data/user.json');
const bodyParser = require('body-parser');  //para enviar el post
var app = express();                // Creamos el servidor de node
app.use(bodyParser.json());
//const PORT = 3000;
var port = process.env.PORT || 3001;


app.use(cors());
app.options('*', cors());


//const mLabURLbase =

const requestJSON = require('request-json');

const mLabURLbase = 'https://api.mlab.com/api/1/databases/techu9db/collections/';
const apiKey = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';


//const PORT = process.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';

//operación GET del 'Hola Mundo'
//Ooperaciòn GET todos los usuarios (users.jason)
//app.get('/holamundo',
//function(request, response) {
//  response.send('Hola Perú!');

///Profe luis:

// GET users a través de mLab
app.get(URL_BASE + 'userjaviDB',
 function(req, res) {
   const httpClient = requestJSON.createClient(mLabURLbase);
   console.log("Cliente HTTP mLab creado.");
   const fieldParam = 'f={"_id":0}&';
  httpClient.get('user?' + fieldParam + apiKey,
     function(err, respuestaMLab, body) {
       console.log('Error: ' + err);
       console.log('Respuesta MLab: ' + respuestaMLab);
       console.log('Body: ' + body);
       var response = {};
       if(err) {
           response = {"msg" : "Error al recuperar users de mLab."}
           res.status(500);
       } else {
         if(body.length > 0) {
           response = body;
         } else {
           response = {"msg" : "Usuario no encontrado."};
           res.status(404);
         }
       }
       res.send(response);
     }); //httpClient.get(
}); //app.get(

//consulta por id
  app.get(URL_BASE + 'userjaviDB/:id',
      function(req, res) {
        const httpClient = requestJSON.createClient(mLabURLbase);
        console.log("Cliente HTTP mLab creado.");
          let ind = req.params.id;
          const fieldParam = 'f={"_id":0}&';//plantillas/interpolacion string
          const fieldString = 'q={"id":' + ind + '}&';
       httpClient.get('user?' + fieldString + fieldParam + apiKey,
      //   client.get('user?' + fieldParam + apikeyMLab,
          function(err, respuestaMLab, body) {
            console.log('Error: ' + err);
            console.log('Respuesta MLab: ' + respuestaMLab);
            console.log('Body: ' + body);
            var response = {};
            if(err) {
                response = {"msg" : "Error al recuperar users de mLab."}
                res.status(500);
            } else {
              if(body.length > 0) {
                response = body;
              } else {
                response = {"msg" : "Usuario no encontrado."};
                res.status(404);
              }
            }
            res.send(response);
          }); //httpClient.get(
    }); //app.get(


//consulta account por id
        app.get(URL_BASE + 'account/:id',
            function(req, res) {
              const httpClient = requestJSON.createClient(mLabURLbase);
              console.log("Cliente HTTP mLab creado.");
                let ind = req.params.id;
                const fieldParam = 'f={"_id":0}&';//plantillas/interpolacion string
                const fieldString = 'q={"id":' + ind + '}&';
             httpClient.get('account?' + fieldString + fieldParam + apiKey,
            //   client.get('user?' + fieldParam + apikeyMLab,
                function(err, respuestaMLab, body) {
                  console.log('Error: ' + err);
                  console.log('Respuesta MLab: ' + respuestaMLab);
                  console.log('Body: ' + body);
                  var response = {};
                  if(err) {
                      response = {"msg" : "Error al recuperar account de mLab."}
                      res.status(500);
                  } else {
                    if(body.length > 0) {
                      response = body;
                    } else {
                      response = {"msg" : "Usuario no encontrado."};
                      res.status(404);
                    }
                  }
                  res.send(response);
                }); //httpClient.get(
          }); //app.get(


//insert
      app.post(URL_BASE + "userjaviDB",
       function(req, res) {
        var  clienteMlab = requestJSON.createClient(mLabURLbase);
        clienteMlab.get('user?'+ apiKey ,
        function(error, respuestaMLab , body) {
            newID=body.length+1;
            console.log("newID:" + newID);
            var newUser = {
              "id" : newID+1,
              "dni": req.body.dni,
              "first_name" : req.body.first_name,
              "last_name" : req.body.last_name,
              "email" : req.body.email,
              "password" : req.body.password
            };
            clienteMlab.post(mLabURLbase + "user?" + apiKey, newUser ,
             function(error, respuestaMLab, body) {
              res.send(body);
           });
        });
      });


      //consulta tarjeta
              app.get(URL_BASE + 'card',
                  function(req, res) {
                    const httpClient = requestJSON.createClient(mLabURLbase);
                    console.log("Cliente HTTP mLab creado.");
                      let card = req.body.cardNum;
                      var pass= req.body.pass;
                      const fieldParam = 'f={"_id":0}&';//plantillas/interpolacion string
                      const fieldString = 'q={"cardNum":"' + card + '"}&';
                   httpClient.get('card?' + fieldString + fieldParam + apiKey,
                  //   client.get('user?' + fieldParam + apikeyMLab,
                      function(err, respuestaMLab, body) {
                        console.log('Error: ' + err);
                        console.log('Respuesta MLab: ' + respuestaMLab);
                        console.log('Body: ' + body);

                        var respuesta = body[0];
                        console.log(respuesta);
                        if(respuesta!=undefined){
                            if (respuesta.pass == pass) {
                              console.log("Clave Correcta");
                              res.send(body[0]);
                            }
                            else {
                              res.send({"msg":"clave incorrecta"});
                            }
                        }else{
                          console.log("Tarjeta Incorrecta");
                          res.send({"msg": "Tarjeta Incorrecta"});
                        }
                      }); //httpClient.get(
                }); //app.get(


// update
      app.put(URL_BASE + 'mov/:idc',
      function(req, res) {
      var  clienteMlab = requestJSON.createClient(mLabURLbase);

      let id = req.params.idc;
      console.log(id);
      const fieldParam = 'f={"_id":0}&';
      const fieldString = 'q={"accountID":' + id + '}&';
       clienteMlab.get('account?'+ fieldString + fieldParam + apiKey ,
       function(error, respuestaMLab , body) {
//         console.log("newID:" + newID);
          console.log("a:" + JSON.stringify(body));
//          var a = JSON.stringify(body[0]);
          var a = body[0];
          console.log(a);
           newID=a.transactions.length+1;
           console.log("newID a:" + newID);

           let newTx = {
                  transacID: newID,
                  transacNum: "2222",
                  date: req.body.date,
                  amount: req.body.amount * -1,
                 description: req.body.description,
                }
           console.log(newTx);
           a.transactions.push(newTx);
           console.log("a:" + "push");
           a.disponible = a.disponible - req.body.amount;
           var cambio = '{"$set":' + JSON.stringify(a) + '}';
           console.log("a:" + cambio);
           clienteMlab.put(mLabURLbase + 'account?q={"accountID": ' + id + '}&' + apiKey, JSON.parse(cambio),
            function(error, respuestaMLab, body) {
              console.log("body a:"+ body);
              res.send(body);
           });
       });
/*
       let id2 = req.body.cuenta;
       const fieldString2 = 'q={"cuenta":"' + id2 + '"}&';
       clienteMlab.get('account?'+ fieldString2 + fieldParam + apiKey ,
       function(error, respuestaMLab , body) {
//         console.log("newID:" + newID);
          console.log("b:" + JSON.stringify(body));
//          var a = JSON.stringify(body[0]);
          var b = body[0];
          console.log(b);
           newID2=b.transactions.length+1;
           console.log("newID b:" + newID2);

           let newTx2 = {
                  transacID: newID2,
                  transacNum: "2222",
                  date: req.body.date,
                  amount: req.body.amount,
                 description: req.body.description,
                }
           console.log(newTx2);
           b.transactions.push(newTx2);
           console.log("b:" + "push");
           b.disponible = b.disponible + req.body.amount;
           var cambio2 = '{"$set":' + JSON.stringify(b) + '}';
           console.log("b:" + cambio2);
           clienteMlab.put(mLabURLbase + 'account?q={"cuenta": "' + id2 + '"}&' + apiKey, JSON.parse(cambio2),
            function(error, respuestaMLab, body) {
              console.log("body b:"+ body);
             res.send(body);
           });
       });
*/
      });


      app.post(URL_BASE + "loginj",
        function (req, res){
          console.log("POST /colapi/v3/login");
          var dni= req.body.dni;
          var pass= req.body.password;
          var queryStringDni='q={"dni":"' + dni + '"}&';
          var queryStringpass='q={"password":' + pass + '}&';
          var  clienteMlab = requestJSON.createClient(mLabURLbase);
          clienteMlab.get('user?'+ queryStringDni+apiKey ,
          function(error, respuestaMLab , body) {
            console.log("entro al body:" + body );
            var respuesta = body[0];
            console.log(respuesta);
            if(respuesta!=undefined){
                if (respuesta.password == pass) {
                  console.log("Login Correcto");
                  var session={"logged":true};
                  var login = '{"$set":' + JSON.stringify(session) + '}';
                  console.log(mLabURLbase+'?q={"id": ' + respuesta.id + '}&' + apiKey);
                  clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(login),
                   function(errorP, respuestaMLabP, bodyP) {
                    res.send(body[0]);
                  });
                }
                else {
                  res.send({"msg":"contraseña incorrecta"});
                }
            }else{
              console.log("Dni Incorrecto");
              res.send({"msg": "Dni Incorrecto"});
            }
          });
      });



      app.post(URL_BASE + "logout",
        function (req, res){
          console.log("POST /colapi/v3/logout");
          var dni= req.body.dni;
          var queryStringDni='q={"dni":"' + dni + '"}&';
          var  clienteMlab = requestJSON.createClient(mLabURLbase);
          clienteMlab.get('user?'+ queryStringDni+apiKey ,
          function(error, respuestaMLab , body) {
            console.log("entro al get");
            var respuesta = body[0];
            console.log(respuesta);
            if(respuesta!=undefined){
                  console.log("logout Correcto");
                  var session={"logged":true};
                  var logout = '{"$unset":' + JSON.stringify(session) + '}';
                  console.log(logout);
                  clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apiKey, JSON.parse(logout),
                   function(errorP, respuestaMLabP, bodyP) {
                    res.send(body[0]);
                    //res.send({"msg": "logout Exitoso Palito"});
                  });
            }else{
              console.log("Error en logout");
              res.send({"msg": "Error en logout"});
            }
          });
      });


///// profe luis

app.listen(port, function(){
  console.log('Node JS escuchando en el puerto otroo666...');
});
